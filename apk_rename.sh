#!/bin/bash
rename() {
	local manifest=$(aapt d badging "$1")
	local label=$(echo $manifest | grep -Po "(?<=application: label=')(.+?)(?=')")
	local package_name=$(echo $manifest | grep -Po "(?<=package: name=').*extension\.(.+?)(?=')" | sed -E 's/.*extension\.(.+)/\1/')
	local version_code=$(echo $manifest | grep -Po "(?<=versionCode=')(.+?)(?=')")
	local version_name=$(echo $manifest | grep -Po "(?<=versionName=')(.+?)(?=')")
	local apk_name="tachiyomi-$package_name-v$version_name.apk"
	local final_name=$(echo $apk_name | tr -d '\\' | tr -d '/')

	#if [ "$apk_name" == " -  -  - .apk" ];	then
	#	echo "🚫  $1 : faild to rename this file"
	#	return
	#fi

	echo
	mv -f "$1" "renamed/$final_name"
	echo "✅ Old name: $1 👇"
	echo "✅ New name: $final_name"
	echo
	echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
	echo
}

for apk in $(pwd)/*.apk
do
	rename "$apk"
done

#rename "Tachiyomi_ Bato.to_1.4.32.apk"
